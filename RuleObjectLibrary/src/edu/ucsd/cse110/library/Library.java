package edu.ucsd.cse110.library;
import java.time.LocalDate;

public class Library {
	List<Publication> pubs;
	List<Member> members;

  void checkoutPublication(Member member, Publication pub) {
    pub.checkout(member, LocalDate.now());
  }
  
  void returnPublication(Publication pub) {
    pub.pubReturn(LocalDate.now());
  }
  
  double getFee(Member member) {
    return member.getDueFees();
  }
  
  bool hasFee(Member member) {
    return member.getDueFees() > 0.01;
  }
}
